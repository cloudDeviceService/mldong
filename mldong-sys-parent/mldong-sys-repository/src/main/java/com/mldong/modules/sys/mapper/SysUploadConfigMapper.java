package com.mldong.modules.sys.mapper;
import com.mldong.modules.sys.entity.SysUploadConfig;
import com.mldong.common.base.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>持久层</p>
 * <p>上传配置</p>
 *
 * @since 2022-04-23 05:26:04
 */
@Repository
public interface SysUploadConfigMapper extends BaseMapper<SysUploadConfig> {

}