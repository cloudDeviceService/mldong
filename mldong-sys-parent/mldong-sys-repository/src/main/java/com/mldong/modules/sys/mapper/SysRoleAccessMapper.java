package com.mldong.modules.sys.mapper;
import com.mldong.modules.sys.entity.SysRoleAccess;
import com.mldong.common.base.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>持久层</p>
 * <p>角色权限关系</p>
 *
 * @since 2022-04-23 05:26:04
 */
@Repository
public interface SysRoleAccessMapper extends BaseMapper<SysRoleAccess> {

}