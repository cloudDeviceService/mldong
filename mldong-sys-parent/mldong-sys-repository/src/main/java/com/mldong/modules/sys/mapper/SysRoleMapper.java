package com.mldong.modules.sys.mapper;
import com.mldong.modules.sys.entity.SysRole;
import com.mldong.common.base.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>持久层</p>
 * <p>角色</p>
 *
 * @since 2022-04-23 05:26:04
 */
@Repository
public interface SysRoleMapper extends BaseMapper<SysRole> {

}