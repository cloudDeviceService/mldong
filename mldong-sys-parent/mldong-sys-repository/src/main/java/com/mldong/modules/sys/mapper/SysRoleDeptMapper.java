package com.mldong.modules.sys.mapper;
import com.mldong.modules.sys.entity.SysRoleDept;
import com.mldong.common.base.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>持久层</p>
 * <p>角色部门关系表</p>
 *
 * @since 2022-04-23 05:26:04
 */
@Repository
public interface SysRoleDeptMapper extends BaseMapper<SysRoleDept> {

}