package com.mldong.modules.sys.dao;
import com.mldong.modules.sys.entity.SysNotice;
import com.mldong.common.base.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>持久层-可自定义层</p>
 * <p>通知公告</p>
 *
 * @since 2022-04-23 05:22:32
 */
@Repository
public interface SysNoticeDao extends BaseMapper<SysNotice> {

}