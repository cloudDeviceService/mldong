package com.mldong.modules.sys.mapper;
import com.mldong.modules.sys.entity.SysNotice;
import com.mldong.common.base.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>持久层</p>
 * <p>通知公告</p>
 *
 * @since 2022-04-23 05:26:04
 */
@Repository
public interface SysNoticeMapper extends BaseMapper<SysNotice> {

}