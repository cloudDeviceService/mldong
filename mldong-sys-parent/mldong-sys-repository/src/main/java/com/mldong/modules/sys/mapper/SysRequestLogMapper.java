package com.mldong.modules.sys.mapper;
import com.mldong.modules.sys.entity.SysRequestLog;
import com.mldong.common.base.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>持久层</p>
 * <p>请求日志</p>
 *
 * @since 2022-04-23 05:26:04
 */
@Repository
public interface SysRequestLogMapper extends BaseMapper<SysRequestLog> {

}