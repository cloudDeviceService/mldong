package com.mldong.modules.sys.dao;
import com.mldong.modules.sys.entity.SysDictItem;
import com.mldong.common.base.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>持久层-可自定义层</p>
 * <p>字典项</p>
 *
 * @since 2022-04-23 05:22:32
 */
@Repository
public interface SysDictItemDao extends BaseMapper<SysDictItem> {

}