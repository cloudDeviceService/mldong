package com.mldong.modules.sys.mapper;
import com.mldong.modules.sys.entity.SysUserRole;
import com.mldong.common.base.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>持久层</p>
 * <p>用户角色关系表</p>
 *
 * @since 2022-04-23 05:26:05
 */
@Repository
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}