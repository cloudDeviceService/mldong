package com.mldong.modules.snakerflow.mapper;
import com.mldong.modules.snakerflow.entity.WfModelDesignerHis;
import com.mldong.common.base.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>持久层</p>
 * <p>模型设计历史</p>
 *
 * @since 2022-05-08 09:18:24
 */
@Repository
public interface WfModelDesignerHisMapper extends BaseMapper<WfModelDesignerHis> {

}