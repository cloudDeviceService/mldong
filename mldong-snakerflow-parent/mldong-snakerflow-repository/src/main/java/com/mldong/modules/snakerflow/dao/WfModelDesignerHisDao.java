package com.mldong.modules.snakerflow.dao;
import com.mldong.modules.snakerflow.entity.WfModelDesignerHis;
import com.mldong.common.base.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>持久层-可自定义层</p>
 * <p>模型设计历史</p>
 *
 * @since 2022-05-08 09:12:53
 */
@Repository
public interface WfModelDesignerHisDao extends BaseMapper<WfModelDesignerHis> {

}