package com.mldong.modules.snakerflow.mapper;
import com.mldong.modules.snakerflow.entity.WfModelDesigner;
import com.mldong.common.base.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>持久层</p>
 * <p>模型设计</p>
 *
 * @since 2022-05-08 09:18:24
 */
@Repository
public interface WfModelDesignerMapper extends BaseMapper<WfModelDesigner> {

}