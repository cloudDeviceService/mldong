package com.mldong.modules.snakerflow.dto;

import com.mldong.common.base.PageParam;
import com.mldong.modules.snakerflow.entity.WfModelGroup;

/**
 * <p>分页查询实体</p>
 * <p>Table: wf_model_group - 模型分组</p>
 * @since 2022-05-08 09:12:53
 */
public class WfModelGroupPageParam extends PageParam<WfModelGroup> {

}
