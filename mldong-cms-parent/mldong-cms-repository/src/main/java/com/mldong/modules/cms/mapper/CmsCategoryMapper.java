package com.mldong.modules.cms.mapper;
import com.mldong.modules.cms.entity.CmsCategory;
import com.mldong.common.base.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>持久层</p>
 * <p></p>
 *
 * @since 2022-04-23 05:32:35
 */
@Repository
public interface CmsCategoryMapper extends BaseMapper<CmsCategory> {

}