package com.mldong.modules.cms.dao;
import com.mldong.modules.cms.entity.CmsCategory;
import com.mldong.common.base.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>持久层-可自定义层</p>
 * <p></p>
 *
 * @since 2022-04-23 05:32:29
 */
@Repository
public interface CmsCategoryDao extends BaseMapper<CmsCategory> {

}